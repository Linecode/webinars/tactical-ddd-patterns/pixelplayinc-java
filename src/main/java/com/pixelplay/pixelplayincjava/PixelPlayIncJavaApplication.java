package com.pixelplay.pixelplayincjava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PixelPlayIncJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PixelPlayIncJavaApplication.class, args);
    }

}
