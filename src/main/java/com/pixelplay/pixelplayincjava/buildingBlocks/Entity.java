package com.pixelplay.pixelplayincjava.buildingBlocks;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
public abstract class Entity<TId> {
    @Setter(AccessLevel.PROTECTED) TId Id;
}
