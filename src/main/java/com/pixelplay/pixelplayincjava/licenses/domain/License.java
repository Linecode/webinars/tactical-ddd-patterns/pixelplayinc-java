package com.pixelplay.pixelplayincjava.licenses.domain;

import com.pixelplay.pixelplayincjava.buildingBlocks.AggregateRoot;
import com.pixelplay.pixelplayincjava.buildingBlocks.DomainEvent;
import com.pixelplay.pixelplayincjava.common.DateRange;
import com.pixelplay.pixelplayincjava.common.DateTimeProvider;
import com.pixelplay.pixelplayincjava.common.RegionIdentifier;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.Licensor;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name="licenses")
@AggregateRoot
public final class License {
    public enum States {Provisioned, Active, Expired}

    @Setter(AccessLevel.PROTECTED)
    @Getter
    @Id
    @GeneratedValue
    private UUID id;

    @Getter
    private States state = States.Provisioned;

    @Getter
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "from", column = @Column(name = "start_date")),
            @AttributeOverride(name = "to", column = @Column(name = "end_date"))
    })
    private DateRange dateRange;

    @Getter
    private UUID licensorId = UUID.fromString("00000000-0000-0000-0000-000000000000");

    @Getter
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "value", column = @Column(name = "region"))
    })
    private RegionIdentifier region;

    @Getter
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "value", column = @Column(name = "decryption_key"))
    })
    private DecryptionKey decryptionKey;

    protected License() {
    }

    private License(UUID id) {
        setId(id);
    }

    public List<DomainEvent> activate(DateTimeProvider provider) throws Exception {
        if (state != States.Provisioned) {
            throw new Exception("License already active");
        }

        if (!dateRange.isWithinRange(provider.getUtcNow())) {
            throw new Exception("License can't be activated yet");
        }

        var event = new Events.LicenseActivated(UUID.randomUUID(), getId(), region);

        apply(event);
        return List.of(event);
    }

    private void apply(Events.LicenseActivated event) {
        state = States.Active;
    }

    public List<DomainEvent> expire() {
        Events.LicenseExpired event = new Events.LicenseExpired(UUID.randomUUID(), getId());

        apply(event);

        return List.of(event);
    }

    private void apply(Events.LicenseExpired event) {
        state = States.Expired;
    }

    private void apply(Events.LicenseCreated event) {
        dateRange = event.getDateRange();
        licensorId = event.getLicensorId();
        region = event.getRegion();
        decryptionKey = DecryptionKey.parse(event.getDecryptionKey());
    }

    public static License create(DateRange dateRange, DecryptionKey decryptionKey, Licensor licensor, RegionIdentifier region) {
        UUID id = UUID.randomUUID();

        Events.LicenseCreated event = new Events.LicenseCreated(
                UUID.randomUUID(),
                id, dateRange,
                licensor.getId(),
                decryptionKey.getValue(),
                region);

        License license = new License(id);
        license.apply(event);

        return license;
    }

    public static class Events {

        @Value
        public static class LicenseCreated extends DomainEvent {
            UUID eventId;
            UUID licenseId;
            DateRange dateRange;
            UUID licensorId;
            String decryptionKey;
            RegionIdentifier region;

            public LicenseCreated(UUID eventId, UUID licenseId, DateRange dateRange, UUID licensorId, String decryptionKey, RegionIdentifier region) {
                this.eventId = eventId;
                this.licenseId = licenseId;
                this.dateRange = dateRange;
                this.licensorId = licensorId;
                this.decryptionKey = decryptionKey;
                this.region = region;
            }
        }

        @Value
        public static class LicenseActivated extends DomainEvent {
            UUID eventId;
            UUID licenseId;
            RegionIdentifier region;

            public LicenseActivated(UUID eventId, UUID licenseId, RegionIdentifier region) {
                this.eventId = eventId;
                this.licenseId = licenseId;
                this.region = region;
            }
        }

        @Value
        public static class LicenseExpired extends DomainEvent {
            UUID eventId;
            UUID licenseId;

            public LicenseExpired(UUID eventId, UUID licenseId) {
                this.eventId = eventId;
                this.licenseId = licenseId;
            }
        }
    }
}
