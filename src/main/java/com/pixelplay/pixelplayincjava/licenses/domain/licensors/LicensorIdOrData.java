package com.pixelplay.pixelplayincjava.licenses.domain.licensors;

import lombok.Value;
import java.util.Optional;
import java.util.UUID;

@Value
public class LicensorIdOrData {
    Optional<UUID> licensorId;
    String name;
    String address;

    public LicensorIdOrData(Optional<UUID> licensorId, String name, String address) {
        if (!licensorId.isPresent() || licensorId.equals(Optional.of(UUID.fromString("00000000-0000-0000-0000-000000000000")))) {
            if (name == null || name.trim().isEmpty()) {
                throw new IllegalArgumentException("Name cannot be empty or whitespace");
            }
            if (address == null || address.trim().isEmpty()) {
                throw new IllegalArgumentException("Address cannot be empty or whitespace");
            }
        }

        this.licensorId = licensorId;
        this.name = name;
        this.address = address;
    }
}