package com.pixelplay.pixelplayincjava.licenses.domain.licensors;

import lombok.Value;
import java.util.UUID;

@Value
public class Licensor {
    UUID id;
    String name;
    String address;

    public Licensor(UUID id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
}
