package com.pixelplay.pixelplayincjava.licenses.domain;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Value;

import java.util.UUID;

@Embeddable
public class DecryptionKey {

    @Getter
    String value;

    protected DecryptionKey() {}

    private DecryptionKey(String value) {
        this.value = value;
    }

    public static DecryptionKey parse(String value) {
        if (value == null || value.length() != 40) {
            throw new IllegalArgumentException("Value must have a length of 40");
        }
        if (!value.startsWith("key")) {
            throw new IllegalArgumentException("Value must start with 'key'");
        }

        String[] parts = value.split("key:");
        if (parts.length < 2 || !isValidUUID(parts[1])) {
            throw new IllegalArgumentException("Value must contain a valid UUID after 'key:'");
        }

        return new DecryptionKey(value);
    }

    private static boolean isValidUUID(String str) {
        try {
            // This will throw an exception if not a valid UUID
            UUID.fromString(str);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}

