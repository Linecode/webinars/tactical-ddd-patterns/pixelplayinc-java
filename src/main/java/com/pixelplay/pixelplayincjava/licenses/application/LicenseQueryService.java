package com.pixelplay.pixelplayincjava.licenses.application;

import com.pixelplay.pixelplayincjava.buildingBlocks.ApplicationService;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicenseRepositoryPort;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicensorProviderPort;
import com.pixelplay.pixelplayincjava.licenses.application.queries.GetLicenseQuery;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.inmemory.licenses.LicenseReadModel;

@ApplicationService
public class LicenseQueryService {

    private final LicenseRepositoryPort licenseRepositoryPort;
    private final LicensorProviderPort licensorProviderPort;

    public LicenseQueryService(LicenseRepositoryPort licenseRepositoryPort, LicensorProviderPort licensorProviderPort) {
        this.licenseRepositoryPort = licenseRepositoryPort;
        this.licensorProviderPort = licensorProviderPort;
    }

    public LicenseReadModel get(GetLicenseQuery query) throws Exception {
        return licenseRepositoryPort.getReadModel(query.getLicenseId(), licensorProviderPort);
    }
}
