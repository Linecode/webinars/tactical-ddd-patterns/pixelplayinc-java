package com.pixelplay.pixelplayincjava.licenses.application;

import com.pixelplay.pixelplayincjava.buildingBlocks.ApplicationService;
import com.pixelplay.pixelplayincjava.common.DateTimeProvider;
import com.pixelplay.pixelplayincjava.licenses.application.commands.ActivateLicenseCommand;
import com.pixelplay.pixelplayincjava.licenses.application.commands.CreateLicenseCommand;
import com.pixelplay.pixelplayincjava.licenses.application.commands.ExpireLicenseCommand;
import com.pixelplay.pixelplayincjava.licenses.application.ports.EventsPublisher;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicenseRepositoryPort;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicensorProviderPort;

import com.pixelplay.pixelplayincjava.licenses.domain.License;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.jpa.JpaLicenseRepositoryAdapter;

import java.util.UUID;


@ApplicationService
public class LicenseService {

    private final LicenseRepositoryPort repository;
    private final LicensorProviderPort licensorProvider;
    private final DateTimeProvider dateTimeProvider;
    private final EventsPublisher eventsPublisher;

    public LicenseService(
            LicenseRepositoryPort repository,
            LicensorProviderPort licensorProvider,
            DateTimeProvider dateTimeProvider,
            EventsPublisher eventsPublisher
    ) {
        this.repository = repository;
        this.licensorProvider = licensorProvider;
        this.dateTimeProvider = dateTimeProvider;
        this.eventsPublisher = eventsPublisher;
    }

    public UUID create(CreateLicenseCommand command) throws Exception {
        var licensor = licensorProvider.getOrCreate(command.getLicensorIdOrData());
        var license = License.create(command.getDateRange(), command.getDecryptionKey(), licensor, command.getRegion());

        repository.add(license);

        return license.getId();
    }

    public void activate(ActivateLicenseCommand command) throws Exception {
        var license = repository.get(command.getLicenseId());

        if (license == null)
            throw new Exception("License not found");

        var events = license.activate(dateTimeProvider);

        eventsPublisher.publish(events);
    }

    public void expire(ExpireLicenseCommand command) throws Exception
    {
        var license = repository.get(command.getLicenseId());

        if (license == null)
            throw new Exception("License not found");

        var events = license.expire();

        eventsPublisher.publish(events);
    }
}

