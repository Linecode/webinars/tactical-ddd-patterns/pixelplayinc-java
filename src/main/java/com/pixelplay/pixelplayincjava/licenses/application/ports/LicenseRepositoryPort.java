package com.pixelplay.pixelplayincjava.licenses.application.ports;

import com.pixelplay.pixelplayincjava.licenses.domain.License;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.inmemory.licenses.LicenseReadModel;

import java.util.UUID;

public interface LicenseRepositoryPort {
    void add(License license);

    void update(License license);

    License get(UUID id);

    LicenseReadModel getReadModel(UUID id, LicensorProviderPort licensorProvider) throws Exception;
}

