package com.pixelplay.pixelplayincjava.licenses.application.commands;

import com.pixelplay.pixelplayincjava.common.DateRange;
import com.pixelplay.pixelplayincjava.common.RegionIdentifier;
import com.pixelplay.pixelplayincjava.licenses.domain.DecryptionKey;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.LicensorIdOrData;
import lombok.Value;

@Value
public class CreateLicenseCommand {
    DateRange dateRange;
    DecryptionKey decryptionKey;
    LicensorIdOrData licensorIdOrData;
    RegionIdentifier region;

    public CreateLicenseCommand(DateRange dateRange, DecryptionKey decryptionKey, LicensorIdOrData licensorIdOrData, RegionIdentifier region) {
        this.dateRange = dateRange;
        this.decryptionKey = decryptionKey;
        this.licensorIdOrData = licensorIdOrData;
        this.region = region;
    }
}
