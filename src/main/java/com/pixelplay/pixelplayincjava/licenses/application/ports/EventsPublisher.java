package com.pixelplay.pixelplayincjava.licenses.application.ports;

import com.pixelplay.pixelplayincjava.buildingBlocks.DomainEvent;

import java.util.Collection;

public interface EventsPublisher {
    <T extends DomainEvent> void publish(Collection<T> events);

    <T extends DomainEvent> void publish(T event);
}
