package com.pixelplay.pixelplayincjava.licenses.application.commands;

import lombok.Value;

import java.util.UUID;

@Value
public class ExpireLicenseCommand {
    UUID licenseId;

    public ExpireLicenseCommand(UUID licenseId) {
        this.licenseId = licenseId;
    }
}
