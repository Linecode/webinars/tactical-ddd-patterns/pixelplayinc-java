package com.pixelplay.pixelplayincjava.licenses.application.ports;

import com.pixelplay.pixelplayincjava.licenses.domain.licensors.Licensor;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.LicensorIdOrData;

public interface LicensorProviderPort {
    Licensor getOrCreate(LicensorIdOrData licensorIdOrData);
}
