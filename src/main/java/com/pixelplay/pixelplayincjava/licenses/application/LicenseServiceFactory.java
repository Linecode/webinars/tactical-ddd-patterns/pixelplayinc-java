package com.pixelplay.pixelplayincjava.licenses.application;

import com.pixelplay.pixelplayincjava.common.DateTimeProvider;
import com.pixelplay.pixelplayincjava.licenses.application.ports.EventsPublisher;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicenseRepositoryPort;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicensorProviderPort;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.jpa.JpaLicenseRepositoryAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LicenseServiceFactory {
    @Bean
    public LicenseService licenseService(
            LicenseRepositoryPort licenseRepositoryPort,
            LicensorProviderPort licensorProviderPort,
            DateTimeProvider dateTimeProvider,
            EventsPublisher eventsPublisher) {
        return new LicenseService(licenseRepositoryPort, licensorProviderPort, dateTimeProvider, eventsPublisher);
    }
}
