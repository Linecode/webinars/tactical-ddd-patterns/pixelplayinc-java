package com.pixelplay.pixelplayincjava.licenses.application.commands;

import lombok.Value;
import java.util.UUID;

@Value
public class ActivateLicenseCommand {
    UUID licenseId;

    public ActivateLicenseCommand(UUID licenseId) {
        this.licenseId = licenseId;
    }
}

