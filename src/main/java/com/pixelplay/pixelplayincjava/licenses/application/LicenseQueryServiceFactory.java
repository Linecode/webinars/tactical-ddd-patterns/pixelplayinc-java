package com.pixelplay.pixelplayincjava.licenses.application;

import com.pixelplay.pixelplayincjava.licenses.application.ports.LicenseRepositoryPort;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicensorProviderPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LicenseQueryServiceFactory {
    @Bean
    public LicenseQueryService licenseQueryService(LicenseRepositoryPort licenseRepositoryPort, LicensorProviderPort licensorProviderPort) {
        return new LicenseQueryService(licenseRepositoryPort, licensorProviderPort);
    }
}
