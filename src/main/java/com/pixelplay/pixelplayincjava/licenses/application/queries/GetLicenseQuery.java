package com.pixelplay.pixelplayincjava.licenses.application.queries;

import lombok.Value;
import java.util.UUID;

@Value
public class GetLicenseQuery {
    UUID licenseId;

    public GetLicenseQuery(UUID licenseId) {
        this.licenseId = licenseId;
    }
}

