package com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.inmemory.licenses;

import com.pixelplay.pixelplayincjava.licenses.domain.License;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.Licensor;
import lombok.Value;
import java.time.LocalDateTime;
import java.util.UUID;

@Value
public class LicenseReadModel {
    UUID id;
    LocalDateTime from;
    LocalDateTime to;
    String decryptionKey;
    int territory;
    String status;
    Licensor licensor;

    public static LicenseReadModel map(License license, Licensor licensor) {
        return new LicenseReadModel(
                license.getId(),
                license.getDateRange().getFrom(),
                license.getDateRange().getTo(),
                license.getDecryptionKey().getValue(),
                license.getRegion().getValue(),
                license.getState().toString(),
                licensor);
    }
}
