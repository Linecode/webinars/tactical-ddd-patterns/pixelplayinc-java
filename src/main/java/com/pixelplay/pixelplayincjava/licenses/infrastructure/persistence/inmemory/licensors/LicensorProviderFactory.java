package com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.inmemory.licensors;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class LicensorProviderFactory {
    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public LicensorProvider licensorProvider() {
        return new LicensorProvider();
    }
}
