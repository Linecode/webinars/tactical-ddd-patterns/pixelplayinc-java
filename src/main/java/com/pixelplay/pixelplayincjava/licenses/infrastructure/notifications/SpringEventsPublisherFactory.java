package com.pixelplay.pixelplayincjava.licenses.infrastructure.notifications;

import com.pixelplay.pixelplayincjava.licenses.application.ports.EventsPublisher;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringEventsPublisherFactory {
    @Bean
    public EventsPublisher eventsPublisher(ApplicationEventPublisher publisher) {
        return new SpringEventsPublisher(publisher);
    }
}
