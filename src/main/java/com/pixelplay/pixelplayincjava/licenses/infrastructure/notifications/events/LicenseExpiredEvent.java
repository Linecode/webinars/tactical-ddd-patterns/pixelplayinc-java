package com.pixelplay.pixelplayincjava.licenses.infrastructure.notifications.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.UUID;

@Getter
public class LicenseExpiredEvent extends ApplicationEvent {
    UUID licenseId;

    public LicenseExpiredEvent(Object source, UUID licenseId) {
        super(source);

        this.licenseId = getLicenseId();
    }
}
