package com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.inmemory.licensors;

import com.pixelplay.pixelplayincjava.licenses.application.ports.LicensorProviderPort;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.Licensor;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.LicensorIdOrData;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LicensorProvider implements LicensorProviderPort {
    private final Map<UUID, Licensor> licensors = new HashMap<>();

    public Licensor getOrCreate(LicensorIdOrData licensorIdOrData) {
        if (licensorIdOrData.getLicensorId().isPresent()) {
            UUID licensorId = licensorIdOrData.getLicensorId().get();
            if (licensors.containsKey(licensorId)) {
                return licensors.get(licensorId);
            }
        }

        UUID id = UUID.randomUUID();
        Licensor licensor = new Licensor(id, licensorIdOrData.getName(), licensorIdOrData.getAddress());

        licensors.put(licensor.getId(), licensor);

        return licensor;
    }
}