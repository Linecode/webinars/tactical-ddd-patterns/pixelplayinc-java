package com.pixelplay.pixelplayincjava.licenses.infrastructure.notifications;

import com.pixelplay.pixelplayincjava.buildingBlocks.DomainEvent;
import com.pixelplay.pixelplayincjava.licenses.application.ports.EventsPublisher;
import com.pixelplay.pixelplayincjava.licenses.domain.License;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.notifications.events.LicenseAcquiredEvent;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.notifications.events.LicenseExpiredEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Collection;

public class SpringEventsPublisher implements EventsPublisher {

    private final ApplicationEventPublisher publisher;

    public SpringEventsPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public <T extends DomainEvent> void publish(Collection<T> events) {
        for (T event : events)
            publish(event);
    }

    @Override
    public <T extends DomainEvent> void publish(T event) {
        var appEvent = mapFromDomainEvent(event);

        if (appEvent != null) {
            publisher.publishEvent(appEvent);
        }
    }

    private ApplicationEvent mapFromDomainEvent(DomainEvent event)
    {
        if (event instanceof License.Events.LicenseActivated casted) {
            return new LicenseAcquiredEvent(this, casted.getLicenseId(), casted.getRegion());
        }

        if (event instanceof  License.Events.LicenseExpired casted) {
            return new LicenseExpiredEvent(this, casted.getLicenseId());
        }

        // Default
        return null;
    }
}
