package com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.jpa;

import com.pixelplay.pixelplayincjava.common.CrudRepository;
import com.pixelplay.pixelplayincjava.licenses.domain.License;

import java.util.UUID;

public interface JpaLicenseRepository extends CrudRepository<License, UUID> {
}
