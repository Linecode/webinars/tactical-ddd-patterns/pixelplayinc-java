package com.pixelplay.pixelplayincjava.licenses.infrastructure.notifications.events;

import com.pixelplay.pixelplayincjava.common.RegionIdentifier;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.UUID;

@Getter
public class LicenseAcquiredEvent extends ApplicationEvent {
    UUID licenseId;
    RegionIdentifier region;

    public LicenseAcquiredEvent(Object source, UUID licenseId, RegionIdentifier region) {
        super(source);

        this.licenseId = licenseId;
        this.region = region;
    }
}
