package com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.jpa;

import com.pixelplay.pixelplayincjava.licenses.application.ports.LicenseRepositoryPort;
import com.pixelplay.pixelplayincjava.licenses.application.ports.LicensorProviderPort;
import com.pixelplay.pixelplayincjava.licenses.domain.License;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.Licensor;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.LicensorIdOrData;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.inmemory.licenses.LicenseReadModel;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class JpaLicenseRepositoryAdapter implements LicenseRepositoryPort {
    private final JpaLicenseRepository repository;

    public JpaLicenseRepositoryAdapter(JpaLicenseRepository repository) {
        this.repository = repository;
    }

    @Override
    public void add(License license) {
        repository.save(license);
    }

    @Override
    public void update(License license) {
        repository.save(license);
    }

    @Override
    public License get(UUID id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public LicenseReadModel getReadModel(UUID id, LicensorProviderPort licensorProvider) throws Exception {
        License license = get(id);

        if (license == null) {
            throw new Exception("License not found");
        }

        Licensor licensor = licensorProvider.getOrCreate(new LicensorIdOrData(Optional.ofNullable(license.getLicensorId()), null, null));

        return LicenseReadModel.map(license, licensor);
    }
}
