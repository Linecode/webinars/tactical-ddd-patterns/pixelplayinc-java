package com.pixelplay.pixelplayincjava.licenses.presentation.rest.resources;

import java.util.Optional;
import java.util.UUID;

public record LicensorResource(Optional<UUID> id, String name, String address) {
}
