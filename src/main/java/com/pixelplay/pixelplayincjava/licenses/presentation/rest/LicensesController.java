package com.pixelplay.pixelplayincjava.licenses.presentation.rest;

import com.pixelplay.pixelplayincjava.common.DateRange;
import com.pixelplay.pixelplayincjava.common.RegionIdentifier;
import com.pixelplay.pixelplayincjava.licenses.application.LicenseQueryService;
import com.pixelplay.pixelplayincjava.licenses.application.LicenseService;
import com.pixelplay.pixelplayincjava.licenses.application.commands.ActivateLicenseCommand;
import com.pixelplay.pixelplayincjava.licenses.application.commands.CreateLicenseCommand;
import com.pixelplay.pixelplayincjava.licenses.application.commands.ExpireLicenseCommand;
import com.pixelplay.pixelplayincjava.licenses.application.queries.GetLicenseQuery;
import com.pixelplay.pixelplayincjava.licenses.domain.DecryptionKey;
import com.pixelplay.pixelplayincjava.licenses.domain.licensors.LicensorIdOrData;
import com.pixelplay.pixelplayincjava.licenses.infrastructure.persistence.inmemory.licenses.LicenseReadModel;
import com.pixelplay.pixelplayincjava.licenses.presentation.rest.resources.CreateLicenseResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class LicensesController {

    private final LicenseService licenseService;
    private final LicenseQueryService licenseQueryService;

    public LicensesController(LicenseService licenseService, LicenseQueryService licenseQueryService) {
        this.licenseService = licenseService;
        this.licenseQueryService = licenseQueryService;
    }

    @PostMapping("/api/license")
    ResponseEntity<String> CreateLicense(@RequestBody CreateLicenseResource request) throws Exception {

        var region = RegionIdentifier.of(request.territory());
        var dateRange = DateRange.of(request.from(), request.to());
        var key = DecryptionKey.parse(request.decryptionKey());
        var licensorIdOrData = new LicensorIdOrData(request.licensor().id(), request.licensor().name(), request.licensor().address());

        var command = new CreateLicenseCommand(dateRange, key, licensorIdOrData, region);

        var id = licenseService.create(command);

        return ResponseEntity.ok()
                .header("Location", "/api/license/" + id)
                .body("");
    }

    @GetMapping("/api/license/{id}")
    ResponseEntity<LicenseReadModel> GetLicense(@PathVariable UUID id) throws Exception {
        var result = licenseQueryService.get(new GetLicenseQuery(id));

        return ResponseEntity.ok()
                .body(result);
    }

    @PostMapping("/api/license/{id}/activate")
    ResponseEntity<String> ActivateLicense(@PathVariable UUID id) throws Exception {
        licenseService.activate(new ActivateLicenseCommand(id));

        return ResponseEntity.ok().body("");
    }

    @PostMapping("/api/license/{id}/expire")
    ResponseEntity<String> ExpireLicense(@PathVariable UUID id) throws Exception {
        licenseService.expire(new ExpireLicenseCommand(id));

        return ResponseEntity.ok().body("");
    }
}
