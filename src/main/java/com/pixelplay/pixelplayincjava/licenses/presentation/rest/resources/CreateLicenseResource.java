package com.pixelplay.pixelplayincjava.licenses.presentation.rest.resources;

import java.time.LocalDateTime;

public record CreateLicenseResource(LocalDateTime from, LocalDateTime to, String decryptionKey, LicensorResource licensor, int territory) { }

