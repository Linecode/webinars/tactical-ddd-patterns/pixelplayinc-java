package com.pixelplay.pixelplayincjava.library.application.listeners;

import com.pixelplay.pixelplayincjava.licenses.infrastructure.notifications.events.LicenseAcquiredEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class LicenseAcquiredListener implements ApplicationListener<LicenseAcquiredEvent> {
    @Override
    public void onApplicationEvent(LicenseAcquiredEvent event) {
        System.out.println("[Library BC]: License Acquired: " + event.getLicenseId());
    }
}
