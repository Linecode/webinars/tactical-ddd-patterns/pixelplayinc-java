package com.pixelplay.pixelplayincjava.common;

import jakarta.persistence.Embeddable;
import lombok.Getter;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.Objects;

@Embeddable
@Getter
public class DateRange {
    private LocalDateTime from = null;
    private LocalDateTime to = null;

    protected DateRange() {}

    protected DateRange(LocalDateTime from, LocalDateTime to) {
        if (from.isAfter(to)) {
            throw new IllegalArgumentException("Date from is greater than date to " + to);
        }
        this.from = from;
        this.to = to;
    }

    public static DateRange of(LocalDateTime from, LocalDateTime to) {
        return new DateRange(from, Objects.requireNonNullElse(to, LocalDateTime.MAX));
    }

    @Override
    public String toString() {
        return from.toString() + " - " + to.toString();
    }

    public boolean isWithinRange(LocalDateTime date) {
        return !date.isBefore(from) && !date.isAfter(to);
    }
}