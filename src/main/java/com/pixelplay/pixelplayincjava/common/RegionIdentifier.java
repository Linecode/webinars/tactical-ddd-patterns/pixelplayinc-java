package com.pixelplay.pixelplayincjava.common;

import jakarta.persistence.Embeddable;
import lombok.Getter;

@Embeddable
@Getter
public class RegionIdentifier {
    private static final int MAX_ID_VALUE = 999;
    private static final int MIN_ID_VALUE = 0;

    short value;

    protected RegionIdentifier() {}

    private RegionIdentifier(short id) {
        if (id < MIN_ID_VALUE || id > MAX_ID_VALUE) {
            throw new IllegalArgumentException("Id must be between 0 and 999");
        }
        this.value = id;
    }

    public static RegionIdentifier of(short id) {
        return new RegionIdentifier(id);
    }

    public static RegionIdentifier of(int id) {
        if (id < MIN_ID_VALUE || id > MAX_ID_VALUE) {
            throw new IllegalArgumentException("Id must be between 0 and 999");
        }
        return new RegionIdentifier((short) id);
    }

    @Override
    public String toString() {
        return String.format("%03d", value);
    }

    public static RegionIdentifier parse(int id) {
        return of(id);
    }

    public static final RegionIdentifier POLAND = of((short) 616);
    public static final RegionIdentifier USA = of((short) 840);
    public static final RegionIdentifier MISSING = of((short) 0);
    public static final RegionIdentifier ALL = of((short) 999);
}
