package com.pixelplay.pixelplayincjava.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DateTimeProviderFactory {
    @Bean
    public DateTimeProvider dateTimeProvider() {
        return new DateTimeProvider();
    }
}
